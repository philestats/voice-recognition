import os
import logging
import sys

import paho.mqtt.client as mqtt
from speech_recognition import Recognizer, Microphone

BOT_NAME="iris"

def forward_to_language_analyzer(mots):
    # TODO: remplacer par publish simple
    mqtt_client = mqtt.Client()
    mqtt_client.connect(mqtt_host, mqtt_port, 60)
    mqtt_client.publish(mqtt_language_analyzer_agent_topic, mots)
    mqtt_client.disconnect()


if __name__ == "__main__":

    NATIVE_PLAYER_CMD="omxplayer"

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s')
    logger = logging.getLogger(__name__)
    mqtt_host = os.getenv("MQTT_HOST")

    all_ok = True

    if not mqtt_host:
        logging.critical("MQTT_HOST env var not found")
        all_ok = False
    else:
        logging.debug(f"MQTT_HOST={mqtt_host}")

    mqtt_port = int(os.getenv("MQTT_PORT"))
    if not mqtt_port:
        logging.critical("MQTT_PORT env var not found")
        all_ok = False
    else:
        mqtt_port = int(mqtt_port)
        logging.debug(f"MQTT_PORT={mqtt_port}")

    mqtt_language_analyzer_agent_topic = os.getenv("TOPIC_AGENT_LANGUAGE_ANALYZER")
    if not mqtt_language_analyzer_agent_topic:
        logging.critical("TOPIC_AGENT_LANGUAGE_ANALYZER env var not found")
        all_ok = False
    else:
        logging.debug(f"TOPIC_AGENT_LANGUAGE_ANALYZER={mqtt_language_analyzer_agent_topic}")

    mqtt_notification_topic = os.getenv("TOPIC_NOTIFICATION")
    if not mqtt_notification_topic:
        logging.critical("TOPIC_NOTIFICATION env var not found")
        all_ok = False
    else:
        logging.debug(f"TOPIC_NOTIFICATION={mqtt_notification_topic}")


    logger.debug("---------------------------------------")
    if not all_ok:
        logger.critical("Some missing envar are missing, the program will not start")
        if mqtt_notification_topic and mqtt_host and mqtt_port is not None:
            error_notification = {"channel": "hermes-status",
                                    "msg": {
                                                "agent": "Agent Fronius",
                                                "status": "Error",
                                                "data": "Some envvars missing"
                                            }
                                  }
            mqtt_send_notification(error_notification)
        sys.exit(1)


    recognizer = Recognizer()
    # On enregistre le son
    with Microphone() as source:
        logger.debug("Réglage du bruit ambiant... Patientez...")
        recognizer.adjust_for_ambient_noise(source)
        logger.info("Vous pouvez parler...")
        while True:
            recorded_audio = recognizer.listen(source)
    
            # Reconnaissance de l'audio
            try:
                text = recognizer.recognize_google(
                  recorded_audio, 
                  language="fr-FR"
                )
                mots = text.lower().split()
                if BOT_NAME in mots:
                    cmd = f"{NATIVE_PLAYER_CMD} /sounds/beep.mp3"
                    os.system(cmd)
                    logger.debug("C'est pour moi")
                    logger.info(f"[ACCEPT] {text}")
                    forward_to_language_analyzer(text)
                else:
                    logger.info(f"[REJECT] {text}")
                    cmd = f"{NATIVE_PLAYER_CMD} /sounds/boop.mp3"
                    # os.system(cmd)
            except Exception as ex:
                print(ex)




