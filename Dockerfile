FROM balenalib/armv7hf-debian

RUN [ "cross-build-start" ]


# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory

RUN apt update && \
    apt install --no-install-recommends -y \
       python3.9-minimal \
       python3-pip \
       python3-pyaudio \
       flac \
       fswebcam

COPY requirements.txt /

RUN pip install --no-cache-dir -r /requirements.txt

# copy the content of the local src directory to the working directory
COPY app/ /app
COPY sounds /sounds

# command to run on container start
CMD [ "python3", "/app/reco.py" ]

RUN [ "cross-build-end" ]

